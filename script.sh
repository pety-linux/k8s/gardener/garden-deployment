#!/bin/bash

# Variables
KUSTOMIZE_GITHUB_PROFILE=kubernetes-sigs
TARGETARCH=amd64
SPIFF_GITHUB_PROFILE=mandelsoft

# Define Functions
install_pkgs() {
apt install -y jq curl findutils apache2-utils git tar bzip2
}

install_spiff() {
curl -L -o spiff-archive.zip https://github.com/${SPIFF_GITHUB_PROFILE}/spiff/releases/download/v1.7.0-beta-4/spiff_linux_${TARGETARCH}.zip
mkdir spiff-extract
unzip -d spiff-extract spiff-archive.zip
cp "spiff-extract/spiff++" /usr/bin/spiff
chmod +x /usr/bin/spiff
rm -rf spiff-archive.zip spiff-extract
}

install_tf() {
wget -q https://releases.hashicorp.com/terraform/1.7.5/terraform_1.7.5_linux_amd64.zip
unzip terraform_1.7.5_linux_amd64.zip
sudo mv terraform /usr/local/bin/terraform
terraform version
}

install_helm() {
curl -L -o helm-archive.tar.gz https://get.helm.sh/helm-v3.12.3-linux-${TARGETARCH}.tar.gz
mkdir helm-extract
tar -xzf helm-archive.tar.gz -C helm-extract
cp helm-extract/linux-${TARGETARCH}/helm /usr/bin/helm
rm -rf helm-archive.tar.gz helm-extract
chmod +x /usr/bin/helm
}

install_kustomize() {
curl -L -o kustomize-archive.tar.gz https://github.com/${KUSTOMIZE_GITHUB_PROFILE}/kustomize/releases/download/kustomize%2Fv5.1.1/kustomize_v5.1.1_linux_${TARGETARCH}.tar.gz
mkdir kustomize-extract
tar -xzf kustomize-archive.tar.gz -C kustomize-extract
cp kustomize-extract/kustomize /usr/bin/kustomize
rm -rf kustomize-archive.tar.gz kustomize-extract
chmod +x /usr/bin/kustomize
}

install_kubectl() {
curl -L -o /usr/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v1.28.5/bin/linux/${TARGETARCH}/kubectl
chmod +x /usr/bin/kubectl
}

# Trigger Functions
install_pkgs
install_spiff
install_tf
install_helm
install_kustomize
install_kubectl