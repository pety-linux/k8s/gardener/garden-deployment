# Gardener deployment

[[_TOC_]]

### General
x

### Prerequisities 
- eksctl
- aws cli
- Docker installed >
- script triggered for preparing developer pc

#### Install eksctl
Ensure that `eksctl` is installed

#### Deploy AWS EKS cluster
```
eksctl create cluster \
--name test-cluster \
--version 1.28 \
--region us-east-1 \
--nodegroup-name cluster-nodes \
--node-type t3.large \
--nodes 4
```

<br>

#### Deploy the Vertical Pod Autoscaler
```
git clone https://github.com/kubernetes/autoscaler.git

cd autoscaler/vertical-pod-autoscaler/

./hack/vpa-up.sh

```

#### Configure AWS EBS csi driver
```
# Enable IAM OIDC provider
eksctl utils associate-iam-oidc-provider --region=us-east-1 --cluster=test-cluster --approve

# Create Amazon EBS CSI driver IAM role
eksctl create iamserviceaccount \
  --region us-east-1 \
  --name ebs-csi-controller-sa \
  --namespace kube-system \
  --cluster test-cluster \
  --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy \
  --approve \
  --role-only \
  --role-name AmazonEKS_EBS_CSI_DriverRole

# Add the Amazon EBS CSI add-on
eksctl create addon --name aws-ebs-csi-driver --cluster test-cluster --service-account-role-arn arn:aws:iam::$(aws sts get-caller-identity --query Account --output text):role/AmazonEKS_EBS_CSI_DriverRole --force
```


More: https://stackoverflow.com/questions/75758115/persistentvolumeclaim-is-stuck-waiting-for-a-volume-to-be-created-either-by-ex/75758116#75758116



```
cd landscape/
cp /root/.kube/config kubeconfig

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/sow/bin
sow order -A

sow deploy -A
sow deploy namespace
```






<br>
<br>

#### Install Docker
```
apt install docker.io
```
<br>

#### Deploy Gardener

##### Install awscli

##### Provide credentials to awscli
```
aws configure
```

##### Install kops
```
# Download and install the kops binary
curl -Lo kops https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
chmod +x kops
sudo mv kops /usr/local/bin/
kops version
```

##### Install cluster
```
# Set variables
export NAME=my-garden.example.com           # The name of the cluster as an FQDN
export KOPS_STATE_STORE=s3://my-kops-bucket # An S3 path to store the cluster state in
export SSH_PUBKEY=~/.ssh/my-key.pub         # An SSH public key to authorize on the nodes

# Prepare config 
kops create cluster \
  --zones=us-east-1a,us-east-1b,us-east-1c \
  --node-count 3 \
  --node-size t3.micro \
  --network-cidr 172.17.0.0/16 \
  --ssh-public-key $SSH_PUBKEY \
  --dry-run \
  --output yaml > cluster.yaml \
  $NAME


# Deploy cluster
kops create cluster \
  --zones=us-east-1a,us-east-1b,us-east-1c \
  --node-count 3 \
  --node-size t3.micro \
  --network-cidr 172.17.0.0/16 \
  --ssh-public-key $SSH_PUBKEY \
  --config cluster.yaml \
  $NAME \
  --yes
```


### Links
- https://devpress.csdn.net/k8s/62ebfc1719c509286f415f79.html
- https://deploy.equinix.com/developers/guides/gardener/
- https://medium.com/@deyagondsamarth/gardener-the-kubernetes-botanist-d7f6e498a9cf
- https://medium.com/@niyazi_erd/kubernetes-clusters-as-a-service-with-gardener-8a4f71412261 [paid]
- https://github.com/gardener/garden-setup

- https://stackoverflow.com/questions/75758115/persistentvolumeclaim-is-stuck-waiting-for-a-volume-to-be-created-either-by-ex/75758116#75758116
